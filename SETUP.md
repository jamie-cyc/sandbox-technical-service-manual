# Sandbox Device Setup

Guidance on initial sandbox device setup.

## Contents

* [Installation Image Creation](#installation-image-creation)
* [Cloning the Repository](#cloning-the-repository)
* [4G Network Connection Keepalive Script](#4g-network-connection-keepalive-script)
* [Slack Post-via-Webhook Script](#slack-post-via-webhook-script)
* [Tor Setup](#tor-setup)
* [qTox Chat Setup](#qtox-chat-setup)

# Installation Image Creation

The `dl-integrity-verify` program should be used in order to download the Xubuntu ISO file, as this will automate the verification process and produce a verifiable audit log.

Review the program [README](https://gitlab.com/jamieweb/dl-integrity-verify/-/blob/master/README.md) including the dependencies and environment requirements. The main one to watch out for is that you have at least `whiptail` **OR** `dialog` to provide the menu interface.

Then, clone the repository, import the required GPG keys, and run the program.

**Make sure to verify the GPG fingerprints for the Ubuntu 2004 and 2012 signing keys!**

    $ git clone https://gitlab.com/jamieweb/dl-integrity-verify.git
    $ cd dl-integrity-verify
    $ gpg2 --import signing-keys/ubuntu-2004 signing-keys/ubuntu-2012
    $ bash ./dl-integrity-verify.sh

Finally, select the latest Xubuntu ISO from the list and wait for it to download and verify.

Double-check that the verification completed successfully, and save a copy of the relevant `VERIFICATIONS` file/audit log, which will be located in the `downloads` folder along with the ISO.

You can then write the image to a USB flash drive, i.e. using 'Restore disk image' in `gnome-disk-utility`, or directly with `dd`.

**Note that once a USB flash drive has been entered into the sandbox environment, it MUST NOT be used again for ANY OTHER PURPOSE outside of the sandbox.**

# Cloning this Repository

Clone the repository:

    $ git clone https://gitlab.com/jamie-cyc/sandbox-technical-service-manual.git

# 4G Network Connection Keepalive Script

To help keep the 4G connection active, the `ping.sh` script should run in the background to send a continuous ping.

Add the following to `crontab -e`:

    @reboot sleep 5 && screen -dmS ping bash ~/sandbox-technical-service-manual/ping.sh

Connect to detached screen session using `screen -r ping`.

# Slack Post-via-Webhook Script

A basic Bash script to post a message to a Slack channel via a webhook.

The entire message will be displayed in a code block by enclosing the content in triple-backticks.

The following characters are 'escaped' using square brackets to prevent accidental hyperlinking, etc:

* `.` (dot)
* `:` (colon)
* `@` (at)
* `` ` `` (backtick)
* `\` (backslash)
* `"` (double quote)
* `%` (percent sign)

The following strings are also replaced for the same reason as above:

* `http` > `hxxp`
* `HTTP` > `HXXP`

### Hook

Your Slack webhook will need to be specified in the `$hook` environment variable.

The 'hook' is defined as everything following `https://hooks.slack.com/services/`.

For example:

    export hook="abc123/def456/ghijkl7891011"

### User ID

You should also specify your user ID/initials using the `$id` environment variable.

### Bash Alias

Finally, you may wish to add a Bash alias to allow easy access to the Slack script, e.g.:

    alias slack="bash ~/sandbox-technical-service-manual/slack.sh"

# Tor Setup

Tor is primarily used to allow qTox to be proxied through Tor. However, it may also be useful during investigations in some situtations.

### Installing Tor from Canonical Universe

Tor is available within the Ubuntu/Canonical 'Universe' repository, but is not reliably patched. If this is acceptable for your activity, then you can install it normally:

    sudo apt install tor

### Installing Tor from Tor's Official Apt Repository

A fully patched version from Tor's official repository is preferable.

Install the HTTPS handler for Apt:

    $ sudo apt install apt-transport-https

Download the Tor repository signing key and **verify the fingerprint**:

    $ wget https://deb.torproject.org/torproject.org/A3C4F0F979CAA22CDBA8F512EE8CBC9E886DDD89.asc

Once you have verified the fingerprint, import the key to Apt:

    $ sudo apt-key add A3C4F0F979CAA22CDBA8F512EE8CBC9E886DDD89.asc

Add the Tor repository address to your Apt sources:

    $ sudo apt-add-repository "https://deb.torproject.org/torproject.org focal main"

Once the Apt cache has finished updating, install the required Tor packages:

    $ sudo apt install tor deb.torproject.org-keyring

### Checking that Tor is Working

You can verify that the local Tor proxy is working by making a request to a Hidden Service, e.g.:

    $ curl --socks5-hostname 127.0.0.1:9050 jamiewebgbelqfno.onion

# qTox Chat Setup

qTox is used to provide a secure chat and file transfer functionality for analysts.

### Installing qTox

Install qTox from the Ubuntu apt repositories:

    sudo apt install qtox

**Note that the `qtox` package is only available in Ubuntu 19.04 onwards.**

### Configuring qTox

Open qTox and configure the following options via the 'Settings -> Advanced' menu:

* Disable IPv6
* Disable UDP
* Disable LAN discovery
* Set proxy type to `SOCKS5`
* Set proxy address to `127.0.0.1`
* Set proxy port to `9050`

This will configure qTox communications to be routed through Tor.

Save the settings and restart qTox to make sure that you're still able to connect. The initial connection can sometimes be a little bit slow.

### Importing Your Tox Profile

Download your Tox profile from the file store and import it to qTox.
