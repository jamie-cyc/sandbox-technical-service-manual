# Sandbox Usage

# Contents

* [Outlook MSG file Conversion](#convert-a-microsoft-outlook-msg-file-into-a-eml-file-readable-with-thunderbird)
* [Decode Base64 Data](#decode-base64-data)
* [Decode URL-Encoded Strings](#decode-url-encoded-strings)
* [Bypass Debugger Statement Abuse/Anti-Analysis](#bypass-debugger-statement-abuseanti-analysis)

# Convert a Microsoft Outlook `.msg` file into a `.eml` file readable with Thunderbird

Unpack the `.msg` file:

    $ munpack -t AttachedMessage.msg

Decode the unpacked base64 data:

    $ base64 -d part1 > message.eml

View email in `less` or Thunderbird:

    $ less message.eml
    $ thunderbird message.eml

# Decode Base64 Data

## Data From File

    $ base64 -d input.txt

Optionally save to file:

    $ base64 -d input.txt > output.txt

## Data From Clipboard

    $ base64 -d

Then paste in encoded data and press `Ctrl+D` to close input.

# Decode URL Encoded Strings

    $ urlencode -d "URL_ENCODED_DATA"

# Bypass Debugger Statement Abuse/Anti-Analysis

Some particularly nasty/aggressive phishing websites will abuse JavaScript `debugger;` statements in order to inhibit the use of the browser developer tools.

The symptoms of this include significant resource consumption, slowness and crashing.

## Firefox

The best mitigation for this within Firefox is to disable the debugger completely, as this doesn't prevent you from using the rest of the browser developer tools.

From `about:config`, set `devtools.debugger.enabled` to `false`.

## Chromium

Within Chromium, access the `Sources` tab of the developer tools and click the `Deactivate breakpoints` button.
