#!/bin/bash
nano --noread ~/message.txt
slackMsg=$(
  printf "Sandbox message from ${id:-unknown ID}:\n\n"
  sed 's/[.]/[.]/g; s/[:]/[:]/g; s/[@]/[@]/g; s/http/hxxp/g; s/HTTP/HXXP/g; s/`/[`]/g; s/[\]/\\\\/g; s/"/\\"/g; s/%/%%/g' ~/message.txt
)
printf "$slackMsg\n\n"
read -p "Press return to send..."
curl -X POST -H "Content-Type: application/json" --data '{"text":"```'"$slackMsg"'```"}' https://hooks.slack.com/services/"$hook"
echo
